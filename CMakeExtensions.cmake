########################################################################################################################
### CMakeExt.cmake
#
# Useful CMake macro and functions.
#
########################################################################################################################
# Defines following private macro:
#	__get_target_filename	: Get the executable filename for the target
#
# Defines following public macro:
#	print_target_size		: Print the output executable size for the specified target
#	add_bin_target			: Build .bin output file for the target
#	add_hex_target			: Build .hex output file for the target

########################################################################################################################
### __get_target_filename
# Get output executable filename from target name.
#
# Args:
#	TARGET_NAME			: Targe name
# Returns:
#	TARGET_FILENAME		: Executable filename

macro(__get_target_filename TARGET_NAME TARGET_FILENAME)
	if(EXECUTABLE_OUTPUT_PATH)
		set(${TARGET_FILENAME} "${EXECUTABLE_OUTPUT_PATH}/${TARGET_NAME}")
	else()
		set(${TARGET_FILENAME} "${TARGET_NAME}")
	endif()
endmacro()

########################################################################################################################
### print_target_size
# Print the output executable file size of the selected target.
#
# Args:
#	TARGET_NAME			: Targe name
macro(print_target_size TARGET_NAME)
	if(NOT DEFINED CMAKE_SIZE)
		message(FATAL_ERROR "CMakeExt: print_target_size: CMAKE_SIZE not defined")
	endif()
	__get_target_filename(${TARGET_NAME} FILENAME)
	add_custom_command(TARGET ${TARGET_NAME} POST_BUILD COMMAND ${CMAKE_SIZE} ${FILENAME})
endmacro()

########################################################################################################################
### add_bin_target
# Build also a .bin output target file from the specified target.
#
# Args:
#	TARGET_NAME			: Targe name
macro(add_bin_target TARGET_NAME)
	if(NOT DEFINED CMAKE_OBJCOPY)
		message(FATAL_ERROR "CMakeExt: build_target_bin: CMAKE_OBJCOPY not defined")
	endif()
	__get_target_filename(${TARGET_NAME} FILENAME)
	add_custom_target("${TARGET_NAME}.bin" ALL DEPENDS ${TARGET_NAME} COMMAND ${CMAKE_OBJCOPY} "-Obinary" ${TARGET_NAME} "${TARGET_NAME}.bin")
endmacro()

########################################################################################################################
### add_hex_target
# Build also a .hex output target file from the specified target.
#
# Args:
#	TARGET_NAME			: Targe name
macro(add_hex_target TARGET_NAME)
	if(NOT DEFINED CMAKE_OBJCOPY)
		message(FATAL_ERROR "CMakeExt: build_target_hex: CMAKE_OBJCOPY not defined")
	endif()
	__get_target_filename(${TARGET_NAME} FILENAME)
	add_custom_target("${TARGET_NAME}.hex" ALL DEPENDS ${TARGET_NAME} COMMAND ${CMAKE_OBJCOPY} "-Obinary" ${TARGET_NAME} "${TARGET_NAME}.hex")
endmacro()
