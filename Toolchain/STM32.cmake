########################################################################################################################
### STM32OOCD.cmake
#
# Set a generic STM32 toolchain file for CMake.

########################################################################################################################

### Check environment variables
if(NOT TOOLCHAIN_PREFIX)
	set(TOOLCHAIN_PREFIX "/usr")
endif()
if(NOT TARGET_TRIPLET)
	set(TARGET_TRIPLET "arm-none-eabi")
endif()

### Toolchain root path
set(CMAKE_SYSROOT "${TOOLCHAIN_PREFIX}/${TARGET_TRIPLET}")
set(CMAKE_INCLUDE_PATH "${CMAKE_SYSROOT}/include")
set(CMAKE_LIBRARY_PATH "${CMAKE_SYSROOT}/lib")
set(CMAKE_PREFIX_PATH "${CMAKE_SYSROOT}/usr")
set(CMAKE_MODULE_PATH "${CMAKE_SYSROOT}/usr/share/cmake/Modules")

### Cross-compilation variables
set(CMAKE_SYSTEM_NAME "Generic")
set(CMAKE_SYSTEM_PROCESSOR "arm")

### Compilers and tools executables
INCLUDE(CMakeForceCompiler)
set(TOOLCHAIN_BIN_PATH "${TOOLCHAIN_PREFIX}/bin")
CMAKE_FORCE_C_COMPILER("${TOOLCHAIN_BIN_PATH}/${TARGET_TRIPLET}-gcc" GNU)
CMAKE_FORCE_CXX_COMPILER("${TOOLCHAIN_BIN_PATH}/${TARGET_TRIPLET}-g++" GNU)
set(CMAKE_ASM_COMPILER "${TOOLCHAIN_BIN_PATH}/${TARGET_TRIPLET}-gcc")
set(CMAKE_AR "${TOOLCHAIN_BIN_PATH}/${TARGET_TRIPLET}-gcc-ar" CACHE FILEPATH "The toolchain ar tool " FORCE)
set(CMAKE_RANLIB "${TOOLCHAIN_BIN_PATH}/${TARGET_TRIPLET}-gcc-ranlib" CACHE FILEPATH "The toolchain ranlib tool " FORCE)
set(CMAKE_NM "${TOOLCHAIN_BIN_PATH}/${TARGET_TRIPLET}-gcc-nm" CACHE FILEPATH "The toolchain nm tool " FORCE)
set(CMAKE_OBJCOPY "${TOOLCHAIN_BIN_PATH}/${TARGET_TRIPLET}-objcopy" CACHE FILEPATH "The toolchain objcopy tool " FORCE)
set(CMAKE_OBJDUMP "${TOOLCHAIN_BIN_PATH}/${TARGET_TRIPLET}-objdump" CACHE FILEPATH "The toolchain objdump tool " FORCE)
set(CMAKE_SIZE "${TOOLCHAIN_BIN_PATH}/${TARGET_TRIPLET}-size" CACHE FILEPATH "The toolchain size tool " FORCE)

### Search paths for libraries/tools
set(CMAKE_FIND_ROOT_PATH "${CMAKE_SYSROOT}")
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

### Compiler flags
set(CMAKE_C_FLAGS "-mabi=aapcs -fomit-frame-pointer -ffunction-sections -fdata-sections -fno-builtin" CACHE INTERNAL "c compiler flags")
set(CMAKE_CXX_FLAGS "-mabi=aapcs -fomit-frame-pointer -ffunction-sections -fdata-sections -fno-builtin" CACHE INTERNAL "cxx compiler flags")
set(CMAKE_ASM_FLAGS "-x assembler-with-cpp" CACHE INTERNAL "asm compiler flags")

### Linker flags
set(CMAKE_EXE_LINKER_FLAGS "-mabi=aapcs -Wl,--gc-sections" CACHE INTERNAL "executable linker flags")
set(CMAKE_MODULE_LINKER_FLAGS "-mabi=aapcs -Wl,--gc-sections" CACHE INTERNAL "module linker flags")
set(CMAKE_SHARED_LINKER_FLAGS "-mabi=aapcs -Wl,--gc-sections" CACHE INTERNAL "shared linker flags")

### Debug flags
set(CMAKE_C_FLAGS_DEBUG "-O0 -g" CACHE INTERNAL "c compiler flags debug")
set(CMAKE_CXX_FLAGS_DEBUG "-O0 -g" CACHE INTERNAL "cxx compiler flags debug")
set(CMAKE_ASM_FLAGS_DEBUG "-g" CACHE INTERNAL "asm compiler flags debug")
set(CMAKE_EXE_LINKER_FLAGS_DEBUG "" CACHE INTERNAL "executable linker flags debug")

### Release build flags
set(CMAKE_C_FLAGS_RELEASE "-Os -DNDEBUG -flto" CACHE INTERNAL "c compiler flags release")
set(CMAKE_CXX_FLAGS_RELEASE "-Os -DNDEBUG -flto" CACHE INTERNAL "cxx compiler flags release")
set(CMAKE_ASM_FLAGS_RELEASE "" CACHE INTERNAL "asm compiler flags release")
set(CMAKE_EXE_LINKER_FLAGS_RELEASE "-flto" CACHE INTERNAL "executable linker flags release")


### RelWithDebInfo build flags
set(CMAKE_C_FLAGS_RELWITHDEBINFO "-O0 -g -DNDEBUG -flto" CACHE INTERNAL "c compiler flags relwithdebinfo")
set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "-O0 -g -DNDEBUG -flto" CACHE INTERNAL "cxx compiler flags relwithdebinfo")
set(CMAKE_ASM_FLAGS_RELWITHDEBINFO "-g " CACHE INTERNAL "asm compiler flags relwithdebinfo")
set(CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO "-flto" CACHE INTERNAL "executable linker flags relwithdebinfo")

### MinSizeRel build flags
set(CMAKE_C_FLAGS_MINSIZEREL "-Os -DNDEBUG -flto" CACHE INTERNAL "c compiler flags minsizerel")
set(CMAKE_CXX_FLAGS_MINSIZEREL "-Os -DNDEBUG -flto" CACHE INTERNAL "cxx compiler flags minsizerel")
set(CMAKE_ASM_FLAGS_MINSIZEREL "" CACHE INTERNAL "asm compiler flags minsizerel")
set(CMAKE_EXE_LINKER_FLAGS_MINSIZEREL "-flto" CACHE INTERNAL "executable linker flags minsizerel")
