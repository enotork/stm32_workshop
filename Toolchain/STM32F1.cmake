########################################################################################################################
### STM32F1.cmake
#
# Add to STM32 the F1 series's specific flags.

########################################################################################################################

### Include STM32 generic flags
include(Toolchain/STM32)

### Add specific compiler flags
set(CMAKE_C_FLAGS "-mthumb -mcpu=cortex-m3 ${CMAKE_C_FLAGS}")
set(CMAKE_CXX_FLAGS "-mthumb -mcpu=cortex-m3 ${CMAKE_CXX_FLAGS}")
set(CMAKE_ASM_FLAGS "-mthumb -mcpu=cortex-m3 ${CMAKE_ASM_FLAGS}")

### Add specific linker flags
set(CMAKE_EXE_LINKER_FLAGS "-mthumb -mcpu=cortex-m3 ${CMAKE_EXE_LINKER_FLAGS}")
set(CMAKE_MODULE_LINKER_FLAGS "-mthumb -mcpu=cortex-m3 ${CMAKE_MODULE_LINKER_FLAGS}")
set(CMAKE_SHARED_LINKER_FLAGS "-mthumb -mcpu=cortex-m3 ${CMAKE_SHARED_LINKER_FLAGS}")
