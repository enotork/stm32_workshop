/**
 * **********************************************************************************************************************
 * @file	scanf.c
 * @author	Ivan Pessotto
 * @version	1.2.0
 * @date	11/03/2016
 * @brief	This file contains scanf implementation for stdio.h
 ***********************************************************************************************************************
 */

// Includes
#include "stdio.h"
#include <stdarg.h>						// Function arguments handling

/**********************************************************************************************************************/

/** @addtogroup UlibC
 * 	@{
 * 		@addtogroup STDIO
 * 		@{
 */

/**********************************************************************************************************************/

#define SCANF_SIGNED				0x01		///< Signed decimal number
#define SCANF_NEGATIVE				0x02		///< Number is negative

/**	@fn static uint8_t scan_int( int32_t* val, uint8_t size, uint8_t base, uint8_t length, uint8_t flags )
 * 	@brief Scan for an integer data value in various forms from buffer.
 * 	@param[out] val  : Pointer to the destination storage integer variable.
 * 	@param[in] size  : Word size of the number to be scanned.
 * 		@arg @c  8   : Byte
 * 		@arg @c 16   : Word
 * 		@arg @c 32   : Double word
 * 	@param[in] base  : Base notation to scan. Can be one of the following values:
 * 		@arg @c 10   : Decimal.
 * 		@arg @c 16   : Hexadecimal.
 * 	@param[in] npads : Number of characters to scan for parsing integer number.
 * 	@param[in] flags : Helping flags for correct number scanning.
 * 	@return Total number of characters scanned.
 */
static uint8_t scan_int( int32_t* val, uint8_t size, uint8_t base, uint8_t length, uint8_t flags ) {
	uint8_t count = 0;
	char ch;
	uint32_t uVal = 0;
	
	// Get first character
	ch = getchar();
	// Decimal with minus? Then it's negative
	// You want a signed number (only for decimal notation) and get a minus as first character?
	if( (flags & SCANF_SIGNED) && ch == '-' ) {
		// Remember it's negative and get next character
		flags |= SCANF_NEGATIVE;
		ch = getchar();
	}
	
	// Loop until we can receive numbers characters
	do {
		// Is a [0-9] character?
		if( ch >= '0' && ch <= '9' )
			uVal = (uVal * base) + ( ch - '0');
		// Is a [a-f] character?
		else if( base == 16 && ch >= 'a' && ch <= 'f' )
			uVal = (uVal * base) + ( ch - 'a' + 10 );
		// Is a [A-F] character?
		else if( base == 16 && ch >= 'A' && ch <= 'F' )
			uVal = (uVal * base) + ( ch - 'A' + 10 );
		// Not a number, end parsing
		else
			break;
		count++;
		length--;
	}
	while( (ch = getchar()) && length > 0 );
	
	// Signed number (only for decimal notation)?
	if( (flags & SCANF_SIGNED) ) {
		// Cast to correct size
		if( size == 32 )
			*val = (int32_t)uVal;
		else if(size == 16)
			*val = (int16_t)uVal;
		else if(size == 8)
			*val = (int8_t)uVal;
		else
			return count;
		// If negative number, invert sign
		if( (flags & SCANF_NEGATIVE))
			*val = -*val;
	}
	// Unsigned or hexadecimal notation
	else {
		// Cast to correct size
		if( size == 32 )
			*val = (uint32_t)uVal;
		else if(size == 16)
			*val = (uint16_t)uVal;
		else if(size == 8)
			*val = (uint8_t)uVal;
		else
			return count;
	}
	
	return count;
}

char getchar( void ) {
	return '\0';
}

uint16_t scanf( const char* format, ... ) {
	va_list args;
	const char* pFor = format;
	uint16_t count = 0;
	uint8_t flags = 0;
	uint8_t length = 0;
	int8_t size = 32;
	char* pStr;
	char ch;
	
	// Start args handles
	va_start( args, format );
	
	// Loop until end of format string
	while( *pFor != '\0' ) {
		// Bypass spaces in format string
		while( *pFor == ' ' )
			pFor++;
		
		// Specifier?
		if( *pFor == '%' ) {
			pFor++;
			// Length
			while( *pFor >= '0' && *pFor <= '9' )
				length = (length * 10) + (*pFor++ - '0');
			// If no length specified, then use maximum length available
			if( length == 0 )
				length--;
			
			// Shorter size (min 8 bit)
			while( *pFor == 'h' && size > 8 ) {
				size /= 2;
				pFor++;
			}
			
			// Check specifier type
			switch( *pFor ) {
				case '%':	// Scan '%'
					if( getchar() != '%' )
						return count;
					break;
				case 'c':	// Character
					*va_arg( args, char* ) = getchar();
					break;
				case 's':	// String
					// Get string pointer
					pStr = va_arg( args, char* );
					do {
						// Get character
						ch = getchar();
						// Is a white space? then stop parsing string
						if( ch == ' ' || ch == '\t' || ch == '\n' || ch == '\v' || ch == '\f' || ch == '\r' )
							break;
						// Add character to string
						*pStr++ = ch;
						count++;
						length--;
					}
					while( length > 0 );
					// End string
					*pStr = '\0';
					break;
				case 'i':	// Signed decimal
					flags |= SCANF_SIGNED;
				case 'u':	// Unsigned decimal
					count += scan_int( va_arg( args, int32_t* ), size, 10, length, flags );
					break;
				case 'p':	// Pointer
					size = 32;
				case 'x':	// Hexadecimal
					count += scan_int( va_arg( args, int32_t* ), size, 16, length, flags );
					break;
				default:	// Error in format string, invalid specifier
					return count;
			}
			// If we are here, then a specifier is parsed correctly
			flags = 0;	// Restore flags
			length = 0;	// Restore no-padding
			size = 32;	// Restore 32-bit size
			pFor++;
		}
		// Matching character
		else if( *pFor == getchar() )
			pFor++;
		// Non-matching character
		else
			return count;
	}
	
	// End args handles
	va_end( args );
	return count;
}

/**********************************************************************************************************************/

/** 	@}
 * 	@}
 */
