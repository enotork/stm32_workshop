/**
 * **********************************************************************************************************************
 * @file	strcmp.c
 * @author	Ivan Pessotto
 * @version	1.0.0
 * @date	10/06/2015
 * @brief	This file contains strcmp implementation for string.h
 ***********************************************************************************************************************
 */

// Includes
#include "string.h"

/**********************************************************************************************************************/

/** @addtogroup UlibC
 * 	@{
 * 		@addtogroup STRING
 * 		@{
 */

/**********************************************************************************************************************/

int8_t strcmp( const char* str1, const char* str2 ) {
	const char* ch1 = str1;
	const char* ch2 = str2;
	char ch;					// Last character from first string, used in while loop
	int8_t diff;				// Difference between characters in str1 and str2
	
	do
		diff = ( ch = *ch1++ ) - *ch2++;
	while( ch != '\0' && diff == 0 );
	return diff;
}

/**********************************************************************************************************************/

/** 	@}
 * 	@}
 */
