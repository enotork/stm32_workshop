/**
 * **********************************************************************************************************************
 * @file	printf.c
 * @author	Ivan Pessotto
 * @version	1.2.0
 * @date	10/03/2016
 * @brief	This file contains printf implementation for stdio.h
 ***********************************************************************************************************************
 */

// Includes
#include "stdio.h"
#include <stdarg.h>						// Function arguments handling

/**********************************************************************************************************************/

/** @addtogroup UlibC
 * 	@{
 * 		@addtogroup STDIO
 * 		@{
 */

/**********************************************************************************************************************/

#define PRINTF_SIGNED				0x01		///< Signed decimal number
#define PRINTF_NEGATIVE				0x02		///< Number is negative
#define PRINTF_PADDING_RIGHT		0x04		///< Right-padding instead of default left-padding

/**	@fn static uint8_t print_int( int32_t val, uint8_t size, int8_t base, uint8_t npads, uint8_t flags )
 * 	@brief Print an integer data value in various forms to buffer.
 * 	@param[in] val   : Integer data value to print.
 * 	@param[in] size  : Word size of the number to be printer.
 * 		@arg @c  8   : Byte
 * 		@arg @c 16   : Word
 * 		@arg @c 32   : Double word
 * 	@param[in] base  : Base notation to print. Can be one of the following values:
 * 		@arg @c  10  : Decimal.
 * 		@arg @c  16  : Hexadecimal.
 * 	@param[in] npads : Number of padding characters to print for printing alignment.
 * 	@param[in] flags : Helping flags for correct number printing.
 * 	@return Total number of characters printed.
 */
static uint8_t print_int( int32_t val, uint8_t size, int8_t base, uint8_t npads, uint8_t flags ) {
	uint8_t count = 0;
	uint32_t uVal;
	char ch;
	char str[12];	// 32-bit = 10 character + 1 for minus
	uint8_t i = 0, j = 0;
	
	// Signed number (only for decimal notation)?
	if( (flags & PRINTF_SIGNED) ) {
		// Cast to correct size
		if( size == 32 )
			val = (int32_t)val;
		else if(size == 16)
			val = (int16_t)val;
		else if(size == 8)
			val = (int8_t)val;
		else
			return count;
		// If negative number, convert to positive and remember it
		if( val < 0) {
			flags |= PRINTF_NEGATIVE;
			val = -val;
		}
		uVal = val;
	}
	// Unsigned or hexadecimal notation
	else {
		// Cast to correct size
		if( size == 32 )
			uVal = (uint32_t)val;
		else if(size == 16)
			uVal = (uint16_t)val;
		else if(size == 8)
			uVal = (uint8_t)val;
		else
			return count;
	}
	
	// Loop until we can divide value
	do {
		// Get remainder of a division of value by base, this is our character to print
		ch = ( char )( uVal % base );
		// Convert value into ASCII character
		ch += ( ch >= 10 ) ? ( 'A' - 10 ) : '0';	// ( ch > 10 ) ? ABCDEF : 0123456789
		// Put character inside string
		str[i++] = ch;
		// Divide by base and go on
		uVal /= base;
	}
	while( uVal > 0 );
	
	// Decimal negative?
	if( base == 10 && (flags & PRINTF_NEGATIVE) ) {
		// Add the minus sign before number
		str[i++] = '-';
		count ++;
	}
	// Hexadecimal ?
	if( base == 16 ) {
		// Extend with zeroes before number
		while( i < size / 4 ) {
			str[i++] = '0';
			count ++;
		}
	}
	
	// Real padding size
	if( npads != 0 )
		npads -= i;
	
	// Left padding (number aligned to right)
	if( ! (flags & PRINTF_PADDING_RIGHT) ) {
		for( j = 0; j < npads; j++ ) {
			putchar( ' ' );
			count ++;
		}
	}
	
	// Send characters of number
	while( i > 0 ) {
		putchar( str[--i] );
		count ++;
	}
	
	// Right padding (number aligned to left)
	if( (flags & PRINTF_PADDING_RIGHT) ) {
		for( j = 0; j < npads; j++ ) {
			putchar( ' ' );
			count ++;
		}
	}
	
	return count;
}

uint16_t printf( const char* format, ... ) {
	va_list args;
	const char* pFor = format;
	uint16_t count = 0;
	uint8_t flags = 0;
	uint8_t npads = 0;
	int8_t size = 32;
	char* pStr;

	// Start args handles
	va_start( args, format );
	
	// Loop until end of format string
	while( *pFor != '\0' ) {
		// Specifier?
		if( *pFor == '%' ) {
			pFor++;
			
			// Padding right (left-align the number)
			if( *pFor  == '-' ) {
				flags |= PRINTF_PADDING_RIGHT;
				pFor++;
			}
			// Padding size
			while( *pFor >= '0' && *pFor <= '9' )
				npads = ( npads * 10 ) + ( *pFor++ - '0' );
			
			// Shorter size (min 8 bit)
			while( *pFor == 'h' && size > 8 ) {
				size /= 2;
				pFor++;
			}
			
			// Check specifier type
			switch( *pFor ) {
				case '%':	// Print '%'
					putchar( '%' );
					count++;
					break;
				case 'c':	// Character
					putchar( va_arg( args, int ) );
					count++;
					break;
				case 's':	// String
					pStr = va_arg( args, char* );
					// Send string
					while( *pStr != '\0' ) {
						putchar( *pStr++ );
						count++;
					}
					break;
				case 'i':	// Signed decimal
					flags |= PRINTF_SIGNED;
				case 'u':	// Unsigned decimal
					count += print_int( va_arg( args, int32_t ), size, 10, npads, flags );
					break;
				case 'p':	// Pointer
					size = 32;
				case 'x':	// Hexadecimal
					count += print_int( va_arg( args, int32_t ), size, 16, npads, flags);
					break;
				default:	// Error in format string, invalid specifier
					return count;
			}
			// If we are here, then a specifier is parsed correctly
			flags = 0;	// Restore flags
			npads = 0;	// Restore no-padding
			size = 32;	// Restore 32-bit size
			pFor++;
		}
		else {
			// Not a specifier character
			putchar( *pFor++ );
			count++;
		}
	}
	
	// End args handles
	va_end( args );
	return count;
}

/**********************************************************************************************************************/

/** 	@}
 * 	@}
 */
