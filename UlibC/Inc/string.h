/**
 * *****************************************************************************
 * @file	string.h
 * @author	Ivan Pessotto
 * @version	v.1.0.0
 * @date	10/06/2015
 * @brief	String manipulation helpers
 * *****************************************************************************
 */

// Prevent recursive inclusion
#ifndef _STRING_H
#define _STRING_H

// Used by C++
#ifdef __cplusplus
extern "C" {
#endif

// Includes
#include <stdint.h>						// Standard types

/**********************************************************************************************************************/

/**	@addtogroup UlibC
 * 	@{
 * 		@defgroup STRING
 * 		@{
 */

/**********************************************************************************************************************/

/**	@fn int8_t strcmp( const char* str1, const char* str2 )
 * 	@brief Compare string pointed to by str1 to the string pointed to str2.
 * 	@param[in] str1 : First string to compare.
 * 	@param[in] str2 : Second string to compare.
 * 	@retval ==0     : If str1 is equal to str2.
 * 	@retval  >0     : If str1 is greather than str2.
 * 	@retval  <0     : If str1 is less than str2.
 */
int8_t strcmp( const char* str1, const char* str2 );

/**********************************************************************************************************************/

/** 	@}
 * 	@}
 */

#ifdef __cplusplus
}		// extern "C" {
#endif

#endif // _STRING_H
