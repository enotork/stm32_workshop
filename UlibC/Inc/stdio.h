/**
 ***********************************************************************************************************************
 * @file	stdio.h
 * @author	Ivan Pessotto
 * @version	v.1.1.0
 * @date	11/03/2016
 * @brief	Standard input/output helpers
 ***********************************************************************************************************************
 */

// Prevent recursive inclusion
#ifndef _STDIO_H
#define _STDIO_H

// Used by C++
#ifdef __cplusplus
extern "C" {
#endif

// Includes
#include <stddef.h>						// Common definitions
#include <stdint.h>						// Standard types

/**********************************************************************************************************************/

/**	@addtogroup UlibC
 * 	@{
 * 		@defgroup STDIO
 * 		@{
 */

/**********************************************************************************************************************/

/**	@fn void putchar( char ch )
 * 	@note Override needed for printf.
 */
void putchar( char ch )__attribute__( ( weak ) );

/**	@fn char getchar( void )
 * 	@note Override needed for scanf.
 */
char getchar( void )__attribute__( ( weak ) );

/**	@fn void printf( const char* format, ... )
 * 	@brief Reduced printf function for use in embedded environment.
 * 	@param[in] format : Formatted string to print with specifiers:
 * 		@arg @c %%    : The '%' character
 * 		@arg @c %c    : Character
 * 		@arg @c %s    : String (char*)
 * 		@arg @c %i    : Decimal notation integer signed 32-bit
 * 		@arg @c %hi   : Decimal notation integer signed 16-bit
 * 		@arg @c %hhi  : Decimal notation integer signed 8-bit
 * 		@arg @c %u    : Decimal notation integer unsigned 32-bit
 * 		@arg @c %hu   : Decimal notation integer unsigned 16-bit
 * 		@arg @c %hhu  : Decimal notation integer unsigned 8-bit
 * 		@arg @c %p    : Hexadecimal notation pointer address 32-bit
 * 		@arg @c %x    : Hexadecimal notation integer 32-bit
 * 		@arg @c %#X   : Pad left the 'X' integer (right aligned) specifier with '#' spaces.
 * 		@arg @c %-#X  : Pad right the 'X' integer (left aligned) specifier with '#' spaces.
 * 	@param[in]  ...   : Optional parameters to match with specifiers in format string.
 * 	@return Total number of characters printed..
 * 	@note Override @ref putchar() function to send the characters to destination peripheral.
 */
uint16_t printf( const char* format, ... );

/**	@fn void scanf( const char* format, ... )
 * 	@brief Reduced scanf function for use in embedded environment.
 * 	@param[in] format : Formatted string to scan with specifiers:
 * 		@arg @c %%    : The '%' character
 * 		@arg @c %c    : Character
 * 		@arg @c %s    : String (char*)
 * 		@arg @c %i    : Decimal notation integer signed 32-bit
 * 		@arg @c %hi   : Decimal notation integer signed 16-bit
 * 		@arg @c %hhi  : Decimal notation integer signed 8-bit
 * 		@arg @c %u    : Decimal notation integer unsigned 32-bit
 * 		@arg @c %hu   : Decimal notation integer unsigned 16-bit
 * 		@arg @c %hhu  : Decimal notation integer unsigned 8-bit
 * 		@arg @c %p    : Hexadecimal notation pointer address 32-bit
 * 		@arg @c %x    : Hexadecimal notation integer 32-bit
 * 		@arg @c %#X   : Parse 'X' specifier using '#' characters.
 * 	@param[out]  ...  : Optional parameters to match with specifiers in format sting.
 * 	@return Total number of characters scanned.
 * 	@note Override @ref getchar() function to get the characters from the source peripheral.
 */
uint16_t scanf( const char* format, ... );

/**********************************************************************************************************************/

/** 	@}
 * 	@}
 */

#ifdef __cplusplus
}		// extern "C" {
#endif

#endif	// _STDIO_H
