########################################################################################################################
### STM32.cmake
#
# Get chip series from defined chip code, then load the corresponding cmake module.
#
########################################################################################################################
# Need following variables:
#	STM32_BOARD				: Board name (like STM3210E_EVAL)
#	STM32_CHIP_NAME			: Chip name (like STM32F103RBT)
#
# Optional variables accepted:
#	WITH_HAL				: Include HAL driver
#	WITH_ULIBC				: Include UlibC library
#
# Defines following variables:
#	STM32_CHIP_SERIES		: STM32 series (ex. F0, F1, F2)
#	STM32_CHIP_TYPE			: STM32 chip type (STM32F103xC, STM32F101xB, ...)
#	STM32_CMSIS_LIBRARY		: Target library for CMSIS_Device
#	STM32_HAL_LIBRARY		: Target library for HAL
#	STM32_BSP_LIBRARY		: Target library for BSP
#	STM32_ULIBC_LIBRARY		: Target library for UlibC
#	STM32_LINKER			: Linker script for the chip type

include(STM32/STM32Cube)
include(STM32/CMakeExtensions OPTIONAL)
include(STM32/OpenOCD OPTIONAL)

########################################################################################################################
### Private function to print variables

function(_DEBUG_OUTPUT_VAL KEY VAL)
	if(${CMAKE_BUILD_TYPE} STREQUAL "Debug")
		string(LENGTH ${KEY} N)
		foreach(I RANGE ${N} 30)
			set(KEY "${KEY} ")
		endforeach()
		message(STATUS "${KEY}: ${VAL}")
	endif()
endfunction()

macro(_DEBUG_OUTPUT_VAR KEY)
	_debug_output_val(${KEY} ${${KEY}})
endmacro()

########################################################################################################################
### Check parameters

if(DEFINED STM32_BOARD)
	_debug_output_val("Board name" ${STM32_BOARD})
	
	### Standardize board's name
	string(TOUPPER ${STM32_BOARD} STM32_BOARD_H)
	
	### Include custom BSPs
	include(STM32/BSP OPTIONAL)
	
	### Get STM32_CHIP_NAME from BSP, if defined
	if(DEFINED BSP_${STM32_BOARD_H}_CHIP_NAME)
		set(STM32_CHIP_NAME ${BSP_${STM32_BOARD_H}_CHIP_NAME})
	endif()
	### Set HAL usage if defined
	if(DEFINED BSP_${STM32_BOARD_H}_USE_HAL)
		set(WITH_HAL 1)
	endif()
endif()

### Check if chip is defined (by board or by user)
if(NOT DEFINED STM32_CHIP_NAME)
	message(FATAL_ERROR "STM32: No STM32_CHIP_NAME defined")
endif()

########################################################################################################################
### Get series amd type of chip

_debug_output_val("Chip name" ${STM32_CHIP_NAME})

### Standardize chip's code
string(TOUPPER ${STM32_CHIP_NAME} STM32_CHIP_NAME})

### Get chip series
string(REGEX REPLACE "^STM32(F[0-4]).+$" "\\1" STM32_CHIP_SERIES ${STM32_CHIP_NAME})
_debug_output_val("Chip series" ${STM32_CHIP_SERIES})
set(STM32_CHIP_SERIES "STM32${STM32_CHIP_SERIES}")

### Get details about this series
if(STM32_CHIP_SERIES STREQUAL "STM32F1")
	set(__CHIP_TYPES
		"00xB" "00xE"
		"01x6" "01xB" "01xE" "01xG"
		"02x6" "02xB"
		"03x6" "03xB" "03xE" "03xG"
		"05xC"
		"07xC"
		)
	set(__CHIP_TYPES_REGEXES
		"00.[468B]" "00.[CDE]"
		"01.[46]" "01.[8B]" "01.[CDE]" "01.[FG]"
		"02.[46]" "02.[8B]"
		"03.[46]" "03.[8B]" "03.[CDE]" "03.[FG]"
		"05.[8BC]"
		"07.[BC]"
		)
	string(REGEX REPLACE "^STM32F1(0[012357].[468BCDEFG]).+$" "\\1" __CHIP_CODE ${STM32_CHIP_NAME})
else()
	message(FATAL_ERROR "STM32: Invalid or unsupported STM32_CHIP_SERIES \"${STM32_CHIP_SERIES}\"")
endif()
### Get chip type from code
foreach(C ${__CHIP_TYPES_REGEXES})
	if(__CHIP_CODE MATCHES ${C})
		list(FIND __CHIP_TYPES_REGEXES ${C} INDEX)
		list(GET __CHIP_TYPES ${INDEX} __CHIP_TYPE)
		break()
	endif()
endforeach()
### Set chip type fullname
set(STM32_CHIP_TYPE "${STM32_CHIP_SERIES}${__CHIP_TYPE}")
_debug_output_val("Chip type" ${STM32_CHIP_TYPE})
add_definitions("-D${STM32_CHIP_TYPE}")

### Set chip type and series in various formats
string(TOLOWER ${STM32_CHIP_SERIES} __STM32_CHIP_SERIES_L)
string(TOLOWER ${STM32_CHIP_TYPE} __STM32_CHIP_TYPE_L)
string(TOUPPER ${STM32_CHIP_TYPE} __STM32_CHIP_TYPE_H)

########################################################################################################################
### Build CMSIS

### Version and pathname
set(_CMSIS_DEVICE_VERSION ${CMSIS_${STM32_CHIP_SERIES}XX_VERSION})
_debug_output_val("CMSIS_Core" "v.${CMSIS_CORE_VERSION}")
_debug_output_val("CMSIS_${STM32_CHIP_SERIES}xx" "v.${_CMSIS_DEVICE_VERSION}")
set(_CMSIS_CORE_PATH "${STM32_SOURCES_PATH}/Drivers/CMSIS")
set(_CMSIS_DEVICE_PATH "${STM32_SOURCES_PATH}/Drivers/CMSIS/Device/ST/${STM32_CHIP_SERIES}xx")

### Includes
include_directories("${_CMSIS_CORE_PATH}/Include" "${_CMSIS_DEVICE_PATH}/Include")

### CMSIS_Device library
set(STM32_CMSIS_LIBRARY "cmsis_${STM32_CHIP_SERIES}xx-${_CMSIS_DEVICE_VERSION}")
add_library(${STM32_CMSIS_LIBRARY} STATIC
	"${_CMSIS_DEVICE_PATH}/Source/Templates/system_${__STM32_CHIP_SERIES_L}xx.c"
	"${_CMSIS_DEVICE_PATH}/Source/Templates/gcc/startup_${__STM32_CHIP_TYPE_L}.s"
	)

### CMSIS linker
set(STM32_LINKER "${_CMSIS_DEVICE_PATH}/Source/Templates/gcc/linker/${__STM32_CHIP_TYPE_H}_FLASH.ld")

########################################################################################################################
### Build HAL

if(DEFINED WITH_HAL)
	### Version and pathname
	set(_HAL_VERSION ${HAL_${STM32_CHIP_SERIES}XX_VERSION})
	_debug_output_val("HAL_${STM32_CHIP_SERIES}xx" "v.${_HAL_VERSION}")
	set(_HAL_PATH "${STM32_SOURCES_PATH}/Drivers/${STM32_CHIP_SERIES}xx_HAL_Driver")
	
	### Includes
	include_directories("${_HAL_PATH}/Inc")
	
	### HAL library
	set(STM32_HAL_LIBRARY "hal_${STM32_CHIP_SERIES}xx-${_HAL_VERSION}")
	file(GLOB _FILES "${_HAL_PATH}/Src/*.c")
	add_library(${STM32_HAL_LIBRARY} STATIC ${_FILES})
endif()

########################################################################################################################
### Build BSP

if(DEFINED STM32_BOARD)
	### Version and pathname
	set(_BSP_VERSION ${BSP_${STM32_BOARD_H}_VERSION})
	_debug_output_val("BSP_${STM32_BOARD}" "v.${_BSP_VERSION}")
	set(_BSP_PATH "${STM32_SOURCES_PATH}/${BSP_${STM32_BOARD_H}_PATH}")
	
	### Includes
	include_directories(${_BSP_PATH})
	
	### BSP library
	set(STM32_BSP_LIBRARY "bsp_${STM32_BOARD}-${_BSP_VERSION}")
	file(GLOB _SOURCES "${_BSP_PATH}/*.c")
	add_library(${STM32_BSP_LIBRARY} STATIC ${_SOURCES})
endif()

########################################################################################################################
### Build UlibC

if(DEFINED WITH_ULIBC)
	### Include package
	include(STM32/UlibC)
	### Version and pathname
	_debug_output_val("UlibC" "v.${ULIBC_VERSION}")
	
	### Includes
	include_directories(${ULIBC_HEADERS_PATH})
	
	### UlibC library
	set(STM32_ULIBC_LIBRARY "ulibc-${ULIBC_VERSION}")
	file(GLOB _SOURCES "${ULIBC_SOURCES_PATH}/*.c")
	add_library(${STM32_ULIBC_LIBRARY} STATIC ${_SOURCES})
endif()
