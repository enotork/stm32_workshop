########################################################################################################################
### STM32CubeF1.cmake
#
# Install STM32CubeF1 sources to system.
#
# You have to configure your package manager to download the firmware source
# package "stm32cubef1.zip" from STM32CubeF1 web page, and unpack the
# source tree in this directory.
# You have to get a folder named "STM32Cube_FW_F1_V1.3.0" inside this directory.
#
# URL: http://www.st.com/st-web-ui/static/active/en/st_prod_software_internet/
#		resource/technical/software/firmware/stm32cubef1.zip
#
# MD5 sum:	4906e18fa744e3dab88b39469ef02b1e
# SHA256:	d7e42253f6e9f605195ce09ad3d5730c381a8764fbfd0878e0eac7978c5d9622
#
########################################################################################################################
# Need following variables:
#	STM32CUBEF1				: Enable or not installation of STM32Cube for this series of chips

if(NOT ${STM32CUBEF1})
	return()
endif()
### Source tree pathnames
set(STM32CUBE_PATH "STM32Cube_FW_F1_V1.3.0")

########################################################################################################################
### CMSIS_CORE

is_newer_version(_RET "CMSIS_CORE_VERSION" "4.5")
if( ${_RET} )
	stm32cube_install("Drivers/CMSIS/Include" "*.h")
endif()

########################################################################################################################
### CMSIS_Device

is_newer_version(_RET "CMSIS_STM32F1XX_VERSION" "4.0.2")
if( ${_RET} )
	stm32cube_install("Drivers/CMSIS/Device/ST/STM32F1xx/Include" "*.h")
	stm32cube_install("Drivers/CMSIS/Device/ST/STM32F1xx/Source/Templates" "*.c")
	stm32cube_install("Drivers/CMSIS/Device/ST/STM32F1xx/Source/Templates/gcc" "*.s")
	stm32cube_install("Drivers/CMSIS/Device/ST/STM32F1xx/Source/Templates/gcc/linker" "*.ld")
endif()

########################################################################################################################
### HAL

is_newer_version(_RET "HAL_STM32F1XX_VERSION" "1.0.2")
if( ${_RET} )
	stm32cube_install("Drivers/STM32F1xx_HAL_Driver/Inc" "*.h" EXCLUDE "stm32f1xx_hal_conf_template.h")
	stm32cube_install("Drivers/STM32F1xx_HAL_Driver/Src" "*.c" EXCLUDE "stm32f1xx_hal_msp_template.c")
endif()

########################################################################################################################
### BSP

stm32cube_install_bsp("STM3210E_EVAL" "6.0.1")
stm32cube_install_bsp("STM3210C_EVAL" "6.0.1")
stm32cube_install_bsp("STM32VL-Discovery" "1.0.1")
stm32cube_install_bsp("STM32F1xx_Nucleo" "1.0.2")
stm32cube_install_bsp("Adafruit_Shield" "1.1.1")
stm32cube_install_bsp_component("Common" "2.2.1")
stm32cube_install_bsp_component("stmpe811" "2.0.0")
stm32cube_install_bsp_component("spfd5408" "1.1.1")
stm32cube_install_bsp_component("hx8347d" "1.1.1")
stm32cube_install_bsp_component("ili9320" "1.2.2")
stm32cube_install_bsp_component("stlm75" "1.0.1")
stm32cube_install_bsp_component("ili9325" "1.2.2")
stm32cube_install_bsp_component("cs43l22" "1.1.0")
stm32cube_install_bsp_component("ak4343" "1.0.0")
stm32cube_install_bsp_component("lis302dl" "1.0.2")
stm32cube_install_bsp_component("st7735" "1.1.1")

########################################################################################################################
### Utilities

stm32cube_install_utils("CPU" "1.1.0")
stm32cube_install_utils("Fonts" "1.0.0")
stm32cube_install_utils("Log" "1.0.0")
