/**
 ***********************************************************************************************************************
 * @file	open103v.h
 * @author	Ivan Pessotto
 * @version	v.1.0.0
 * @date	24/02/2016
 * @brief	This file contains definitions for Open103V board by Waveshare:
 * - Leds
 * - Buttons
 * @see		http://www.waveshare.com/open103v-standard.htm
 ***********************************************************************************************************************
 */

// Prevent recursive inclusion
#ifndef __OPEN103V_H
#define __OPEN103V_H

// Used by C++
#ifdef __cplusplus
extern "C" {
#endif

// Includes
#include <stm32f1xx_hal.h>

/**********************************************************************************************************************/

/** @addtogroup BSP
 * 	@{
 * 		@defgroup Open103V
 * 		@{
 */

/** @def USE_OPEN103V
 * 	@brief Define for Open103V board
 */ 
#if !defined (USE_OPEN103V)
#define USE_OPEN103V
#endif

#define HSE_VALUE				((uint32_t)8000000)		///< Value of the high-speed external oscillator in Hz
#define LSE_VALUE				((uint32_t)32768)		///< Value of the low-speed external oscillator in Hz
#define VDD_VALUE				((uint32_t)3300)		///< Value of VDD in mV

/**********************************************************************************************************************/

/**			@defgroup Open103V_Leds Leds
 * 			@{
 */

/** @typedef Led_TypeDef
 * 	- @c LED1
 * 	- @c LED2
 * 	- @c LED3
 * 	- @c LED4
 */
typedef enum {
	LED1 = 0,
	LED2 = 1,
	LED3 = 2,
	LED4 = 3
} Led_TypeDef;

/** @fn void BSP_Led_Init( Led_TypeDef Led )
 * 	@brief LED BSP initialization
 * 	@param[in] led : Led to initialize, one of @ref Led_TypeDef
 * 	@return None
 */
void BSP_Led_Init( Led_TypeDef led );

/** @fn void BSP_Led_DeInit( Led_TypeDef Led )
 * 	@brief LED BSP de-initialization
 * 	@param[in] led : Led to initialize, one of @ref Led_TypeDef
 * 	@return None
 */
void BSP_Led_DeInit( Led_TypeDef led );

/** @fn void BSP_Led_On( Led_TypeDef led )
 * 	@brief Power on a LED
 * 	@param[in] led : Led to light up, one of @ref Led_TypeDef
 * 	@return None
 */
void BSP_Led_On( Led_TypeDef led );

/** @fn void BSP_Led_Off( Led_TypeDef led )
 * 	@brief Power off a LED
 * 	@param[in] led : Led to shut down, one of @ref Led_TypeDef
 * 	@return None
 */
void BSP_Led_Off( Led_TypeDef led );

/** @fn void BSP_Led_Toggle( Led_TypeDef led )
 * 	@brief Toggle state of a LED
 * 	@param[in] led : Led to toggle, one of @ref Led_TypeDef
 * 	@return None
 */
void BSP_Led_Toggle( Led_TypeDef led );

/**			@}
 */

/**********************************************************************************************************************/

/**			@defgroup Open103V_Buttons Buttons
 * 			@{
 */

/** @typedef Button_TypeDef
 * 	@brief Button names
 * 	- @c USER_KEY       : User push-button
 * 	- @c JOYSTICK_RIGHT : Right Push Button on Joystick
 * 	- @c JOYSTICK_UP    : Up Push Button on Joystick
 * 	- @c JOYSTICK_LEFT  : Left Push Button on Joystick
 * 	- @c JOYSTICK_DOWN  : Down Push Button on Joystick
 * 	- @c JOYSTICK_ENTER : Sel Push Button on Joystick
 */
typedef enum {
	USER_KEY       = 0,
	JOYSTICK_RIGHT = 1,
	JOYSTICK_UP    = 2,
	JOYSTICK_LEFT  = 3,
	JOYSTICK_DOWN  = 4,
	JOYSTICK_ENTER = 5
} Button_TypeDef;

/** @typedef ButtonMode_TypeDef
 * 	@brief Type of input pin
 * 	- @c BUTTON_MODE_GPIO : Button will be used as simple input
 * 	- @c BUTTON_MODE_EVT  : Button will be connected to EXTI line with interrupt generation capability
 * 	- @c BUTTON_MODE_EVT  : Button will be connected to EXTI line with event generation capability
 */
typedef enum {
	BUTTON_MODE_GPIO = 0,
	BUTTON_MODE_EXTI = 1,
	BUTTON_MODE_EVT  = 2
} ButtonMode_TypeDef; 

/** @typedef ButtonState_TypeDef
 * 	@brief Type of input pin
 * 	- @c BUTTON_RELEASED : Button is released (input is 1)
 * 	- @c BUTTON_PRESSED  : Button is pressed (input is 0)
 */
typedef enum {
	BUTTON_RELEASED = 0,
	BUTTON_PRESSED  = 1
} ButtonState_TypeDef;

/** @typedef JoyState_TypeDef
 * 	@brief Abstract value of joystick group of pushbuttons.
 * 	- @c JOYSTICK_NONE  : None pressed
 * 	- @c JOYSTICK_RIGHT : Press to the right
 * 	- @c JOYSTICK_UP    : Press to up
 * 	- @c JOYSTICK_LEFT  : Press to the left
 * 	- @c JOYSTICK_DOWN  : Press down
 * 	- @c JOYSTICK_SEL   : Press toward the center of the joystick
 */
typedef enum {
	JOYSTICK_IS_NONE  = 0,
	JOYSTICK_IS_RIGHT = 1,
	JOYSTICK_IS_UP    = 2,
	JOYSTICK_IS_LEFT  = 3,
	JOYSTICK_IS_DOWN  = 4,
	JOYSTICK_IS_ENTER = 5
} JoyState_TypeDef;

/** @fn void BSP_Button_Init( Button_TypeDef button, ButtonMode_TypeDef buttonMode )
 * 	@brief Button BSP initialization
 * 	@param[in] button     : Button to initialize, one of @ref Button_TypeDef
 * 	@param[in] buttonMode : Button's working mode, one of @ref ButtonMode_TypeDef
 * 	@return None
 */
void BSP_Button_Init( Button_TypeDef button, ButtonMode_TypeDef buttonMode );

/** @fn void BSP_Button_DeInit( Button_TypeDef button );
 * 	@brief Button BSP de-initialization
 * 	@param[in] button : Button to initialize, one of @ref Button_TypeDef
 * 	@return None
 */
void BSP_Button_DeInit( Button_TypeDef button );

/** @fn ButtonState_TypeDef BSP_Button_GetState( Button_TypeDef button )
 * 	@brief Button BSP get state
 * 	@param[in] button : Button to get state, one of @ref Button_TypeDef
 * 	@return State of push-button, one of @ref ButtonState_TypeDef
 */
ButtonState_TypeDef BSP_Button_GetState( Button_TypeDef button );

/** @fn void BSP_Joystick_Init( ButtonMode_TypeDef joyMode )
 * 	@brief Joystick BSP initialization
 * 	@param[in] joyMode : Input type, one of @ref ButtonMode_TypeDef
 * 	@return None
 */
void BSP_Joystick_Init( ButtonMode_TypeDef joyMode );

/** @fn void BSP_Joystick_DeInit( void );
 * 	@brief Joystick BSP de-initialization
 * 	@return None
 */
void BSP_Joystick_DeInit( void );

/** @fn JoyState_TypeDef BSP_Joystick_GetState( void )
 * 	@brief Joystick BSP get state
 * 	@return One of the @ref JoyState_TypeDef values
 */
JoyState_TypeDef BSP_Joystick_GetState( void );

/**			@}
 */

/**********************************************************************************************************************/

/**		@}
 * 	@}
 */

#ifdef __cplusplus
}		// extern "C" {
#endif

#endif	// __OPEN103V_H
