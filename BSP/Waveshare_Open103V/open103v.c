/**
 * **********************************************************************************************************************
 * @file	open103v.c
 * @brief	This file contains implementations for open103v.h
 ***********************************************************************************************************************
 */

// Includes
#include "open103v.h"

/**********************************************************************************************************************/

/** @addtogroup BSP
 * 	@{
 * 		@addtogroup Open103V
 * 		@{
 */

/**********************************************************************************************************************/

/** 		@addtogroup Open103V_Leds
 * 			@{
 */

/** @def IS_BSP_LED(__LED__)
 * 	@brief Check if parameter is one of @ref Led_TypeDef
 */
#define IS_BSP_LED(__LED__) ( \
	((__LED__) == LED1) || \
	((__LED__) == LED2) || \
	((__LED__) == LED3) || \
	((__LED__) == LED4) \
)

#define LEDn							4
#define LED1_PIN						GPIO_PIN_12			// PB12
#define LED2_PIN						GPIO_PIN_13			// PB13
#define LED3_PIN						GPIO_PIN_14			// PB14
#define LED4_PIN						GPIO_PIN_15			// PB15
#define LEDx_PORT						GPIOB
#define LEDx_GPIO_CLK_ENABLE()			__HAL_RCC_GPIOB_CLK_ENABLE()
#define LEDx_GPIO_CLK_DISABLE()			__HAL_RCC_GPIOB_CLK_DISABLE()

/** @var const uint16_t LED_PIN[LEDn]
 * 	@brief Constant array of pin numbers for the board's leds
 * 	- @c LED1_PIN : PB12
 * 	- @c LED2_PIN : PB13
 * 	- @c LED3_PIN : PB14
 * 	- @c LED4_PIN : PB15
 */
const uint16_t LED_PIN[LEDn] = {
	LED1_PIN,
	LED2_PIN, 
	LED3_PIN,
	LED4_PIN
};

/**	@fn void BSP_Led_Init( Led_TypeDef led )
 * 	Initialize the GPIO pin and port for a specific output LED
 */
void BSP_Led_Init( Led_TypeDef led ) {
	GPIO_InitTypeDef  gpioinitstruct = { 0 };
	
	// Check parameters
	assert_param(IS_BSP_LED(led));
	
	// Enable the clock
	LEDx_GPIO_CLK_ENABLE();
	
	// Configure the GPIO pins
	gpioinitstruct.Pin    = LED_PIN[led];
	gpioinitstruct.Mode   = GPIO_MODE_OUTPUT_PP;
	gpioinitstruct.Pull   = GPIO_NOPULL;
	gpioinitstruct.Speed  = GPIO_SPEED_FREQ_HIGH;
	
	// Enable the led output pin
	HAL_GPIO_Init(LEDx_PORT, &gpioinitstruct);
	// Shut down led
	HAL_GPIO_WritePin(LEDx_PORT, LED_PIN[led], GPIO_PIN_RESET);
}

/**	@fn void BSP_Led_DeInit( Led_TypeDef led )
 * 	Frees the GPIO resources for the LED pin
 */
void BSP_Led_DeInit( Led_TypeDef led ) {
	// Check parameters
	assert_param(IS_BSP_LED(led));
	
	// Disable led output pin
	HAL_GPIO_DeInit(LEDx_PORT, LED_PIN[led]);
}

/**	@fn void BSP_Led_On( Led_TypeDef led )
 */
void BSP_Led_On( Led_TypeDef led ) {
	// Check parameters
	assert_param(IS_BSP_LED(led));
	
	HAL_GPIO_WritePin(LEDx_PORT, LED_PIN[led], GPIO_PIN_SET);
}

/**	@fn void BSP_Led_Off( Led_TypeDef led )
 */
void BSP_Led_Off( Led_TypeDef led ) {
	// Check parameters
	assert_param(IS_BSP_LED(led));
	
	HAL_GPIO_WritePin(LEDx_PORT, LED_PIN[led], GPIO_PIN_RESET);
}

/**	@fn void BSP_Led_Toggle( Led_TypeDef led )
 */
void BSP_Led_Toggle( Led_TypeDef led ) {
	// Check parameters
	assert_param(IS_BSP_LED(led));
	
	HAL_GPIO_TogglePin(LEDx_PORT, LED_PIN[led]);
}

/** 		@}
 */

/**********************************************************************************************************************/

/** 		@addtogroup Open103V_Buttons
 * 			@{
 */

/** @def IS_BSP_BUTTON(__BUTTON__)
 * 	@brief Check if parameter is one of @ref Button_TypeDef
 */
#define IS_BSP_BUTTON(__BUTTON__) ( \
	((__BUTTON__) == USER_KEY) || \
	((__BUTTON__) == RIGHT_JOYSTICK) || \
	((__BUTTON__) == UP_JOYSTICK) || \
	((__BUTTON__) == LEFT_JOYSTICK) || \
	((__BUTTON__) == DOWN_JOYSTICK) || \
	((__BUTTON__) == SEL_JOYSTICK) \
)

/** @def IS_BSP_BUTTON_MODE(__MODE__)
 * 	@brief Check if parameter is one of @ref ButtonMode_TypeDef
 */
#define IS_BSP_BUTTON_MODE(__MODE__) ( \
	((__MODE__) == BUTTON_MODE_GPIO) || \
	((__MODE__) == BUTTON_MODE_EXTI) || \
	((__MODE__) == BUTTON_MODE_EVT) \
)

#define BUTTONn							6
#define USER_KEY_PIN					GPIO_PIN_0			// PC0
#define USER_KEY_PORT					GPIOC
#define USER_KEY_GPIO_CLK_ENABLE()		__HAL_RCC_GPIOC_CLK_ENABLE()
#define USER_KEY_GPIO_CLK_DISABLE()		__HAL_RCC_GPIOC_CLK_DISABLE()
#define USER_KEY_EXTI_IRQn				EXTI0_IRQn
#define BUTTONx_GPIO_CLK_ENABLE(__BUTTON__) do { \
	if ((__BUTTON__) == USER_KEY) USER_KEY_GPIO_CLK_ENABLE(); \
} while(0)
#define BUTTONx_GPIO_CLK_DISABLE(__BUTTON__) ( \
	((__BUTTON__) == USER_KEY) ? USER_KEY_GPIO_CLK_DISABLE() : 0 \
)

#define JOYSTICK_RIGHT_PIN				GPIO_PIN_0			// PA0
#define JOYSTICK_RIGHT_PORT				GPIOA
#define JOYSTICK_RIGHT_EXTI_IRQn		EXTI0_IRQn
#define JOYSTICK_UP_PIN					GPIO_PIN_1			// PC1
#define JOYSTICK_UP_PORT				GPIOC
#define JOYSTICK_UP_EXTI_IRQn			EXTI1_IRQn
#define JOYSTICK_LEFT_PIN				GPIO_PIN_2			// PC2
#define JOYSTICK_LEFT_PORT				GPIOC
#define JOYSTICK_LEFT_EXTI_IRQn			EXTI2_IRQn
#define JOYSTICK_DOWN_PIN				GPIO_PIN_3			// PC3
#define JOYSTICK_DOWN_PORT				GPIOC
#define JOYSTICK_DOWN_EXTI_IRQn			EXTI3_IRQn
#define JOYSTICK_ENTER_PIN				GPIO_PIN_1			// PA1
#define JOYSTICK_ENTER_PORT				GPIOA
#define JOYSTICK_ENTER_EXTI_IRQn		EXTI1_IRQn
#define JOYSTICK_GPIO_CLK_ENABLE() { \
	__HAL_RCC_GPIOA_CLK_ENABLE(); \
	__HAL_RCC_GPIOC_CLK_ENABLE(); \
}
#define JOYSTICK_GPIO_CLK_DISABLE() { \
	__HAL_RCC_GPIOA_CLK_DISABLE(); \
	__HAL_RCC_GPIOC_CLK_DISABLE(); \
}

/** @var const uint16_t Button_Pin[BUTTONn]
 * 	@brief Constant array of pin numbers for the buttons
 * 	- @c USER_KEY_PIN       : PC0
 * 	- @c JOYSTICK_RIGHT_PIN : PA0
 * 	- @c JOYSTICK_UP_PIN    : PC1
 * 	- @c JOYSTICK_LEFT_PIN  : PC2
 * 	- @c JOYSTICK_DOWN_PIN  : PC3
 * 	- @c JOYSTICK_ENTER_PIN : PA1
 */
const uint16_t Button_Pin[BUTTONn] = {
	USER_KEY_PIN,
	JOYSTICK_RIGHT_PIN,
	JOYSTICK_UP_PIN,
	JOYSTICK_LEFT_PIN, 
	JOYSTICK_DOWN_PIN,
	JOYSTICK_ENTER_PIN
};

/** @var GPIO_TypeDef* Button_Port[BUTTONn]
 * 	@brief Constant array of ports name for the buttons
 * 	- @c USER_KEY_PORT       : C
 * 	- @c JOYSTICK_RIGHT_PORT : A
 * 	- @c JOYSTICK_UP_PORT    : C
 * 	- @c JOYSTICK_LEFT_PORT  : C
 * 	- @c JOYSTICK_DOWN_PORT  : C
 * 	- @c JOYSTICK_ENTER_PORT : A
 */
GPIO_TypeDef* Button_Port[BUTTONn] = {
	USER_KEY_PORT,
	JOYSTICK_RIGHT_PORT,
	JOYSTICK_UP_PORT,
	JOYSTICK_LEFT_PORT,
	JOYSTICK_DOWN_PORT,
	JOYSTICK_ENTER_PORT
};

/** @var const uint8_t Button_IRQn[BUTTONn]
 * 	@brief Constant array of IRQ line number for every buttons
 * 	- @c USER_KEY_EXTI_IRQn       : EXTI0_IRQn
 * 	- @c JOYSTICK_RIGHT_EXTI_IRQn : EXTI0_IRQn
 * 	- @c JOYSTICK_UP_EXTI_IRQn    : EXTI1_IRQn
 * 	- @c JOYSTICK_LEFT_EXTI_IRQn  : EXTI2_IRQn
 * 	- @c JOYSTICK_DOWN_EXTI_IRQn  : EXTI3_IRQn
 * 	- @c JOYSTICK_ENTER_EXTI_IRQn : EXTI1_IRQn
 */
const uint8_t Button_IRQn[BUTTONn] = {
	USER_KEY_EXTI_IRQn,
	JOYSTICK_RIGHT_EXTI_IRQn,
	JOYSTICK_UP_EXTI_IRQn,
	JOYSTICK_LEFT_EXTI_IRQn,
	JOYSTICK_DOWN_EXTI_IRQn,
	JOYSTICK_ENTER_EXTI_IRQn
};

/**	@fn void BSP_Button_Init( Button_TypeDef button, ButtonMode_TypeDef buttonMode )
 * 	Initialize the GPIO pin and port and EXTI line for a button
 */
void BSP_Button_Init( Button_TypeDef button, ButtonMode_TypeDef buttonMode ) {
	GPIO_InitTypeDef gpioinitstruct = {0};
	
	// Check parameters
	assert_param(IS_BSP_BUTTON(button));
	assert_param(IS_BSP_BUTTON_MODE(buttonMode));
	
	// Enable the clock
	BUTTONx_GPIO_CLK_ENABLE(button);
	
	// Configure button's pin as input
	gpioinitstruct.Pin   = Button_Pin[button];
	gpioinitstruct.Pull  = GPIO_PULLUP;
	gpioinitstruct.Speed = GPIO_SPEED_FREQ_HIGH;
	
	// Standard GPIO input button
	if (buttonMode == BUTTON_MODE_GPIO)	{
		// Configure pin as input
		gpioinitstruct.Mode = GPIO_MODE_INPUT;
		HAL_GPIO_Init(Button_Port[button], &gpioinitstruct);
	}
	// Interrupt input button
	else if (buttonMode == BUTTON_MODE_EXTI) {
		// User-key push-button
		if(button == USER_KEY)
			// External interrupt on rising edge
			gpioinitstruct.Mode = GPIO_MODE_IT_RISING;
		// Joystick
		else
			// External interrupt on falling edge
			gpioinitstruct.Mode = GPIO_MODE_IT_FALLING;
		// Initialize the button's pin
		HAL_GPIO_Init(Button_Port[button], &gpioinitstruct);
		
		// Enable and set EXTI interrupt to the lowest priority
		HAL_NVIC_SetPriority((IRQn_Type)(Button_IRQn[button]), 0x0F, 0);
		HAL_NVIC_EnableIRQ((IRQn_Type)(Button_IRQn[button]));
	}
	// Event input button
	else if (buttonMode == BUTTON_MODE_EVT) {
		if(button != USER_KEY)
			// External event on rising edge
			gpioinitstruct.Mode = GPIO_MODE_EVT_RISING;
		else
			// External event on falling edge
			gpioinitstruct.Mode = GPIO_MODE_EVT_FALLING;
		// Initialize the button's pin
		HAL_GPIO_Init(Button_Port[button], &gpioinitstruct);
	}
}

/**	@fn void BSP_Button_DeInit( Button_TypeDef button )
 * 	Revert GPIO configuration to it's default state
 */
void BSP_Button_DeInit( Button_TypeDef button ) {
	// Check parameters
	assert_param(IS_BSP_BUTTON(button));

	// Free buttons input pin
	HAL_GPIO_DeInit(Button_Port[button], Button_Pin[button]);
}

/**	@fn ButtonState_TypeDef BSP_Button_GetState( Button_TypeDef button )
 * 	Get state of a push-button
 */
ButtonState_TypeDef BSP_Button_GetState( Button_TypeDef button ) {
	GPIO_PinState pinstate;
	
	// Check parameters
	assert_param(IS_BSP_BUTTON(button));
	
	// Get input pin state
	pinstate = HAL_GPIO_ReadPin(Button_Port[button], Button_Pin[button]);
	
	return ((pinstate == GPIO_PIN_RESET) ? BUTTON_PRESSED : BUTTON_RELEASED);
}

/**	@fn void BSP_Joystick_Init( ButtonMode_TypeDef joyMode )
 * 	Initialize the GPIO pins and ports and EXTI lines for the joystick's buttons
 */
void BSP_Joystick_Init( ButtonMode_TypeDef joyMode ) {
	// Check parameters
	assert_param(IS_BSP_BUTTON_MODE(joyMode));
	
	// Enable the joystick clock
	JOYSTICK_GPIO_CLK_ENABLE();
	
	// Inizialize all joystick's buttons
	BSP_Button_Init(JOYSTICK_RIGHT, joyMode);
	BSP_Button_Init(JOYSTICK_UP, joyMode);
	BSP_Button_Init(JOYSTICK_LEFT, joyMode);
	BSP_Button_Init(JOYSTICK_DOWN, joyMode);
	BSP_Button_Init(JOYSTICK_ENTER, joyMode);
}

/**	@fn void BSP_Joystick_DeInit( void )
 * 	Revert GPIO configuration to it's default state
 */
void BSP_Joystick_DeInit( void ) {
	// De-inizialize all joystick's buttons
	BSP_Button_DeInit(JOYSTICK_RIGHT);
	BSP_Button_DeInit(JOYSTICK_UP);
	BSP_Button_DeInit(JOYSTICK_LEFT);
	BSP_Button_DeInit(JOYSTICK_DOWN);
	BSP_Button_DeInit(JOYSTICK_ENTER);
}

/**	@fn JoyState_TypeDef BSP_Joystick_GetState( void )
 * 	Returns the current joystick status
 */
JoyState_TypeDef BSP_Joystick_GetState( void ) {
	JoyState_TypeDef joystate = JOYSTICK_IS_NONE;

	// Check all buttons for the one wich is pressed
	(BSP_Button_GetState(JOYSTICK_RIGHT) == BUTTON_PRESSED) ? joystate = JOYSTICK_IS_RIGHT : 0 ;
	(BSP_Button_GetState(JOYSTICK_UP) == BUTTON_PRESSED) ? joystate = JOYSTICK_IS_UP : 0 ;
	(BSP_Button_GetState(JOYSTICK_LEFT) == BUTTON_PRESSED) ? joystate = JOYSTICK_IS_LEFT : 0 ;
	(BSP_Button_GetState(JOYSTICK_DOWN) == BUTTON_PRESSED) ? joystate = JOYSTICK_IS_DOWN : 0 ;
	(BSP_Button_GetState(JOYSTICK_ENTER) == BUTTON_PRESSED) ? joystate = JOYSTICK_IS_ENTER : 0 ;
	
	return joystate;
}

/**			@}
 */

/**********************************************************************************************************************/

/**		@}
 * 	@}
 */
